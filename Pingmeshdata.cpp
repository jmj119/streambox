/*
 * hymL this is used to read PingmeshData to PingmeshRecord
 * compile: g++ --std=c++11 -o pingmeshdata pingmeshdata.cpp.
 * */
#if 0
#include <iostream>
#include <stdint.h>
#include <stdlib.h>
#include <cassert>
#include <string>
#include <cstring>
#include <unordered_map>
#include <fstream>
#include <string.h>
#include <sstream>
#include <fstream>
using namespace std;

#define SERVER_NAME_LENGTH 64
#define LEVEL_LENGTH 16
#define ENTRY_NUMBER 14
#define NAME_RECORD_LENGTH 11

//struct PingmeshRecord
typedef struct
{
	uint32_t     srcClusterId;
	uint32_t     dstClusterId;
	uint32_t     rtt;
	uint32_t     errCode;
	
	uint32_t     ver;
	time_t       ts;
	char         srcServerName[SERVER_NAME_LENGTH];
	int32_t      srcId;
	uint32_t     srcIp;
	uint16_t     srcPort;
	char         dstServerName[SERVER_NAME_LENGTH];
	uint32_t     dstId;
	uint32_t     dstIp;
	uint16_t     dstPort;
	uint32_t     seq;
	uint32_t     type;
	char         level[LEVEL_LENGTH];
	uint32_t     msgLength;
}PingmeshRecord;


/**
 *      * Sequence number assigned to unique server Id, starting from 1.
 *           */
static unsigned int    g_serverSeq; 

/**
 *     * Sequence number assigned to unique cluster Id, starting from 1.
 *         */
static unsigned int    g_clusterSeq;

unordered_map<string, unsigned int> serverName2Id;

unordered_map<unsigned int, string> serverId2Name;

unordered_map<string, unsigned int> clusterName2Id;

unordered_map<unsigned int, string> clusterId2Name;

/**
 *      * It is one to one mapping from server id to cluster id. 
 *           * The reverse is not true.
 *                */
unordered_map<unsigned int, unsigned int> serverId2ClusterId;
#endif

#include "Pingmeshdata.h"

bool Pingmeshdata::SplitString(char *line, char dem, char** ppArray, int arrayLen)
{
	char *pStart = line;
	char *pEnd = line;
	int index = 0;

	//assume the string ends with '\0'
		while (*pEnd != '\0')
		{
			if (*pEnd == dem )
			{
				*pEnd = '\0';

				if (pStart != pEnd)
				{
					ppArray[index++] = pStart;
				}
				else
				{
					ppArray[index++] = NULL;
				}

				pEnd++;
				pStart = pEnd;

				if (index == arrayLen)
				{
					break;
				}
			}
			else
			{
				pEnd++;
			}
		}

	if (index < arrayLen - 2)
	{
		ppArray[0] = NULL;
		return false;
	}

	if (pStart != pEnd)
	{
		ppArray[arrayLen - 1] = pStart;
	}

	return true;
}




void Pingmeshdata::BuildNameMaps(const char *azNodesFile)
{
	ifstream ifs(azNodesFile, std::ifstream::in | std::ifstream::binary);
	if (ifs.is_open() == false)
	{	
		std::cout << "cannot open " << azNodesFile << "!!!!!!!!!!!!!" << std::endl;
		return;
	}

	char line[256] = { 0 };
	char *record[NAME_RECORD_LENGTH] = { 0 };

	//#logicalName	sku	logicalIp	deviceType	cluster	env	mf	scaleUnit	tor	physicalName	physicalIp
	while (ifs.getline(line, 256))
	{
		if (line[0] == '#')
		{
			continue;
		}

		if (SplitString(line, '\t', record, NAME_RECORD_LENGTH))
		{
			if (record[5] == NULL || record[9] == NULL || record[3] == NULL)
			{
				continue;
			}

			//if (_stricmp(record[3], "SERVER") == 0)
			if (strcmp(record[3], "SERVER") == 0)
			{
				string serverName = record[9];
				string clusterName = record[5];

				unsigned id, cid;

				unordered_map<string, unsigned int>::iterator it, cit; 

				it = serverName2Id.find(serverName);
				if (it == serverName2Id.end())
				{
					id =  g_serverSeq++;
					serverName2Id[serverName] = id;
					serverId2Name[id] = serverName;
				}
				else
				{
					id = (*it).second;
				}

				cit = clusterName2Id.find(clusterName);
				if (cit == clusterName2Id.end())
				{
					cid = g_clusterSeq++;
					clusterName2Id[clusterName] = cid;
					clusterId2Name[cid] = clusterName;
				}
				else
				{
					cid = (*cit).second;
				}

				serverId2ClusterId[id] = cid;
			}
		}
	}

	cout << "Total server: " << serverName2Id.size() << ", total clusters: " << clusterName2Id.size() << endl;
	ifs.close();
}


unsigned int Pingmeshdata::GetServerIdByName(const char *name)
{
	unordered_map<string, unsigned int>::iterator it; 
	it = serverName2Id.find(name);

	if (it == serverName2Id.end())
	{
		return 0;
	}

	return (*it).second;
}


unsigned int Pingmeshdata::GetClusterIdByServerId(unsigned int id)
{
	unordered_map<unsigned int, unsigned int>::iterator it;
	it = serverId2ClusterId.find(id);

	if (it == serverId2ClusterId.end())
	{
		return 0;
	}

	return (*it).second;
}



void Pingmeshdata::PingmeshRecordParser(char *line, PingmeshRecord * ppRecord)
{
	//1,10/25/2016 21:08:48,BL2X3CIS160451,100.65.229.132,60152,BL2X3CIS260725,100.79.74.130,1101,209,524,0,2,Intra-DC,0
	char *pStart = line;
	char *pEnd = line;
	int index = 0;
	char *parray[ENTRY_NUMBER] = { 0 };

	while (*pEnd != '\0')
	{
		if (*pEnd == ',' || *pEnd == '\r')
		{
			*pEnd = '\0';

			if (pStart != pEnd)
			{
				parray[index++] = pStart;
			}
			else
			{
				parray[index++] = NULL;
			}

			if (index == ENTRY_NUMBER)
			{
				break;
			}

			pEnd++;
			pStart = pEnd;
		}
		else
		{
			pEnd++;
		}
	}

	if (index < ENTRY_NUMBER - 2)
	{
		//*ppRecord = NULL;
		ppRecord = NULL;
		return;
	}

	if (pStart != pEnd)
	{
		parray[ENTRY_NUMBER - 1] = pStart;
	}
/*	
	for(int i = 0; i < ENTRY_NUMBER; i++){
		std::cout << "parray[" << i << "] is: " << parray[i] << std::endl;
	}
*/
	if (ppRecord != NULL)
	{
		
		ppRecord->rtt = atoi(parray[9]);
		//std::cout << "rtt is " << ppRecord->rtt << std::endl;
		ppRecord->errCode = atoi(parray[10]);
		//std::cout << "errCode is " << ppRecord->errCode << std::endl;
		std::strcpy(ppRecord->srcServerName, parray[2]);
		//std::cout << "srcServerName is " << ppRecord->srcServerName << std::endl;
		std::strcpy(ppRecord->dstServerName, parray[5]);
		//std::cout << "dstServerName is " << ppRecord->dstServerName << std::endl;
		ppRecord->srcId = GetServerIdByName(ppRecord->srcServerName);
		//std::cout << "srcId is " << ppRecord->srcId << std::endl;
		ppRecord->srcClusterId = GetClusterIdByServerId(ppRecord->srcId);
		//std::cout << "srcClusterId is " << ppRecord->srcClusterId << std::endl; 
		ppRecord->dstId = GetServerIdByName(ppRecord->dstServerName);
		//std::cout << "dstId is " << ppRecord->dstId << std::endl;
		ppRecord->dstClusterId = GetClusterIdByServerId(ppRecord->dstId);
		//std::cout << "dstClusterId is " << ppRecord->dstClusterId << std::endl; 
#if 0
		(*ppRecord)->ver = atoi(parray[0]);
		(*ppRecord)->ts = StringToTime(parray[1]);
		strcpy_s((*ppRecord)->srcServerName, parray[2]);
		InetPtonA(AF_INET, parray[3], &(*ppRecord)->srcIp);
		(*ppRecord)->srcPort = ntohs((u16)(atoi(parray[4])));
		strcpy_s((*ppRecord)->dstServerName, parray[5]);
		InetPtonA(AF_INET, parray[6], &(*ppRecord)->dstIp);
		(*ppRecord)->dstPort = ntohs((u16)(atoi(parray[7])));
		(*ppRecord)->seq = atoi(parray[8]);
		(*ppRecord)->rtt = atoi(parray[9]);
		(*ppRecord)->errCode = atoi(parray[10]);
		(*ppRecord)->type = atoi(parray[11]);
		strcpy_s((*ppRecord)->level, parray[12]);
		(*ppRecord)->msgLength = atoi(parray[13]);

		(*ppRecord)->srcId = pMaps->GetServerIdByName((*ppRecord)->srcServerName);
		(*ppRecord)->dstId = pMaps->GetServerIdByName((*ppRecord)->dstServerName);
		(*ppRecord)->srcClusterId = pMaps->GetClusterIdByServerId((*ppRecord)->srcId);
		(*ppRecord)->dstClusterId = pMaps->GetClusterIdByServerId((*ppRecord)->dstId);

#ifdef STREAM_DEBUG
		if ((*ppRecord)->srcId == 0 || (*ppRecord)->dstId == 0 || (*ppRecord)->srcClusterId == 0 || (*ppRecord)->dstClusterId == 0)
		{
			cerr << "A record with id 0: " << (*ppRecord)->srcServerName << " " << (*ppRecord)->dstServerName;
			cerr << " " << (*ppRecord)->srcId << " " << (*ppRecord)->dstId << " " << (*ppRecord)->srcClusterId << " " << (*ppRecord)->dstClusterId << endl;
		}
#endif 
#endif
	}

}
#if 0
int main(){
	char line[] = "1,10/25/2016 21:08:48,BL2X3CIS160451,100.65.229.132,60152,BL2X3CIS260725,100.79.74.130,1101,209,524,0,2,Intra-DC,0"; 
	PingmeshRecord *p = NULL;	
	p = (PingmeshRecord*)malloc(sizeof(PingmeshRecord));
	assert(p);
	BuildNameMaps("/ssd/azNodes.tsv");
	PingmeshRecordParser(line, p);
	std::cout << sizeof(PingmeshRecord) << std::endl;
	std::cout << "rtt is " << p->rtt << std::endl;
	std::cout << "errCode is " << p->errCode << std::endl;
	std::cout << "srcServerName is " << p->srcServerName << std::endl;
	std::cout << "dstServerName is " << p->dstServerName << std::endl;
	std::cout << "srcClusterId is " << p->srcClusterId << std::endl; 
	std::cout << "dstClusterId is " << p->dstClusterId << std::endl; 

	std::string line1;
	std::ifstream infile("/ssd/region_Raw_PingmeshData_100.log");
	while(std::getline(infile, line1)){
		std::cout << line1 << std::endl;
		PingmeshRecordParser(line, p);
		std::cout << "dstClusterId is " << p->dstClusterId << std::endl;
	}

	return 0;
}
#endif
