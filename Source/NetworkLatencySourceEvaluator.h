#ifndef NETWORKLATENCYSOURCEEVALUATOR_H
#define NETWORKLATENCYSOURCEEVALUATOR_H 
extern "C" {
#include "measure.h"
}

#include "core/TransformEvaluator.h"
#include "core/EvaluationBundleContext.h"
#include "NetworkLatencySource.h"
#include <omp.h>
//#include <hbwmalloc.h>
class NetworkLatencySourceEvaluator
	: public TransformEvaulator<NetworkLatencySource>{
	using T = struct srcdst_rtt;
	//using OutputBundleT = RecordBundle<pair<creek::ippair, long>>;
	//using OutputBundleT = RecordBundle<clusterid_rtt>;
	using OutputBundleT = RecordBundle_test;
	using TransformT = NetworkLatencySource;

public:
	ptime current_ts;
	NetworkLatencySourceEvaluator(int node) : current_ts(boost::gregorian::date(2016, Jan, 1)){
		//current_ts = boost::posix_time::microsec_clock::local_time();
	}
	
	void evaluate(NetworkLatencySource* t, EvaluationBundleContext *c,
      		shared_ptr<BundleBase> bundle_ptr = nullptr) override{
/*	
		//test hbw_malloc function
		char * a = (char *) hbw_malloc(16);
		if(a == NULL){
			std::cout << "hbw_malloc failed.............." << std::endl;
		}else{
			std::cout << "hbw_malloc succeed!!!!!!!!!!!!!!" << std::endl;
			hbw_free(a);
		}
*/
		//output_bundle->add_record(Record<KVPair>(KVPair(str, 1), in.ts));	
		auto out = t->getFirstOutput();
		assert(out);

		/* # of bundles between two puncs */
		const uint64_t bundle_per_interval
			= 1 * numa_num_configured_cpus(); /* # cores */

		boost::posix_time::time_duration punc_interval =
			milliseconds(t->punc_interval_ms);

		boost::posix_time::time_duration delta =
			milliseconds(t->punc_interval_ms) / bundle_per_interval;

		//    const int bundle_count = 2;	// debugging
		const uint64_t records_per_bundle
			= t->records_per_interval / bundle_per_interval;

		EE(" ---- punc internal is %d sec (ev time) --- ",
				t->punc_interval_ms / 1000);
	
//		const int num_nodes = numa_max_node() + 1;
		const int num_nodes = 1;
		uint64_t us_per_iteration =
		  1e6 * t->records_per_interval / t->target_tput; /* the target us */
		uint64_t offset = 0;
		static atomic<uint64_t> offset1 (0);
	
		////hym: chanage it to other values
		//const int total_tasks = 20;
		const int total_tasks = t->source_threads;
		vector <std::future<void>> futures;

		//total_tasks
		//OutputBundleT ** bundle_ptr1 = new OutputBundleT * [64]; // = new shared_ptr<RecordBundle<pair<creek::ippair, long>>>[2];
		//bundle_ptr1[0] = new OutputBundleT;
		//bundle_ptr1[0]->add_record(t->record_buffers[0][0]);
		
		//OutputBundleT ** bundle_ptr2 = new OutputBundleT * [64][2]; // = new shared_ptr<RecordBundle<pair<creek::ippair, long>>>[2];
		const int iteration = 5;
		const uint64_t local_bundle_per_interval = 2;
		//OutputBundleT * bundle_ptr2[iteration][total_tasks][local_bundle_per_interval]; //iteration, threads, bundle_per_interval
		//uint64_t local_records_per_bundle = t->record_num/total_tasks/local_bundle_per_interval;
		//note: each bundle size is 114405 now
		//114405 = t->record_num/64/2; can change this later
		uint64_t local_records_per_bundle = t->record_num/64/local_bundle_per_interval;
		delta = milliseconds(t->punc_interval_ms) / local_bundle_per_interval;
	
		std::cout << "========================" << std::endl;
		std::cout << "total records in buffer: " << t->record_num << std::endl;
		std::cout << "divide total records into 64 parts:" << std::endl;
		std::cout << "local_bundle_per_interval: " << local_bundle_per_interval << std::endl;
		std::cout << "local_records_per_bundle: " << local_records_per_bundle << std::endl;
		std::cout << "iteration is: " << iteration << std::endl;
		std::cout << "#loop * total_tasks * local_bundle_per_interval * iteration < 5000: must be true.." << std::endl;
		
		std::cout << "total_tasks in source is " << total_tasks << std::endl;
		std::cout << "========================" << std::endl;


/*
		OutputBundleT *tmp;
		for(int i = 0; i < 10; i++){
			std::cout << "=========== get bundle " << i <<std::endl;
			tmp = c->get_bundle();
		}
		tmp->add_record(t->record_buffers[0][0]);
*/
		std::cout << std::endl << std::endl << std::endl << std::endl;
/*
		for(int i = 0; i < total_tasks; i++){
			bundle_ptr1[i] = new OutputBundleT(local_records_per_bundle);
			for(int m = 0; m < local_records_per_bundle; m++){
				bundle_ptr1[i]->add_record(t->record_buffers[0][0]); //init all elements
			}
			bundle_ptr1[i]->rewind();
		}
*/
		//while(true){
		//for(int s = 0; s < 10; s++){
		for(int s = 0; s < 20; s++){
		boost::posix_time::ptime start_tick = 
			boost::posix_time::microsec_clock::local_time();

#if 0
//hym: This is the simplest case for injection rate: just read record from buffer, and test the memory read bandwidth 
////////////////////////////////////////////////////////////////////
		int nthreads = 32;

		start_tick = boost::posix_time::microsec_clock::local_time();

		for(int k = 0; k < 50; k++){	
			#pragma omp parallel for num_threads(nthreads)
			for(int i = 0; i < t->record_num; i++){
				t->record_buffers[0][i].data.first++;
				t->record_buffers[0][i].data.second++;
			}
		}
		boost::posix_time::ptime end_tick = 
			boost::posix_time::microsec_clock::local_time();
		auto elapsed_us = (end_tick - start_tick).total_microseconds();
		assert(elapsed_us > 0);

		std::cout << "nthreads is: " << nthreads << std::endl;
		std::cout << "record size: " << sizeof(t->record_buffers[0][0] ) << std::endl;
		std::cout << "total size: " << 50 * sizeof(t->record_buffers[0][0]) * t->record_num/1024/1024 << "MB" <<std::endl;
		std::cout << "total records: " << 50 * t->record_num << std::endl;
		std::cout << "total time: " << elapsed_us << " us" << std::endl;
		//std::cout << "tput is: " << sizeof(t->record_buffers[0][0] * t->record_num)/1024/1024/1024(elapsed_us/1000/1000) << "GB/s" << std::endl;
		std::cout << "bw is: " << 50 * sizeof(t->record_buffers[0][0]) * t->record_num/(elapsed_us * 1024) << "GB/s" << std::endl;
		std::cout << "tput is: " << 50 * t->record_num * 1000 /elapsed_us << "Rec/s" << std::endl;
/////////////////////////////////////////////////////////////////////
#endif

#if 0
//////////////////////////////////////////////////////////////////
//hym: something wrong with the block
//hum: offset is not a variable in openmp for loop, so we get some wired results
//This block is used to test memory bandwidth
//It shoudl be around 19GB/s on KNL's slow memory
			int nthreads = 64; 
			int64_t bundle_per_interval_test = 100;
			int64_t records_per_bundle_test = 1024 * 1024;
		        
			int record_size = sizeof(t->record_buffers[0][0].data);
			std::cout << "data size is: " << sizeof(t->record_buffers[0][0].data) << std::endl;
			std::cout << "record_num: " << t->record_num << std::endl;
			std::cout << "record_num * record_size = " << t->record_num * record_size/1024/1024 << "MB" << std::endl;
			#pragma omp parallel for
			//for (unsigned int i = 0; i < bundle_per_interval_test; i++) {
			//for (unsigned int i = 0; i < bundle_per_interval_test * records_per_bundle_test; i++) {
				for(unsigned int j = 0; j < records_per_bundle_test; j++, offset++){
					if((int64_t)offset >= t->record_num){ //hym: offset is not opemp's for-loop variable, remember to midify here. Ref the code above
						offset = 0;
					}
					//sd_ip = t->record_buffers[0][offset].data.sd_ip;
					//rtt += t->record_buffers[0][offset].data.second; 
					rtt += t->record_buffers[0][offset].data.second;
					ipid = t->record_buffers[0][offset].data.first;
					offset ++;
				//} //end for j
			}//end for i

			boost::posix_time::ptime end_tick = 
				boost::posix_time::microsec_clock::local_time();
			std::cout << "rtt is " << rtt << std::endl;	
			std::cout << "ipid is " << ipid << std::endl;
			auto elapsed_us = (end_tick - start_tick).total_microseconds();
			assert(elapsed_us > 0);
			
			std::cout << "elapsed_us: " << elapsed_us << " us" << std::endl;
			std::cout << "records: " << records_per_bundle_test * bundle_per_interval_test << std::endl;
			std::cout << "total size: " << (records_per_bundle_test * bundle_per_interval_test * record_size)/1024/1024/1024 << " GB" << std::endl;
			//std::cout << "throughput: " << (records_per_bundle_test * bundle_per_interval_test * 40)/1024/1024/(elapsed_us/1000/1000)/1024 << "GB/s" << std::endl;
			std::cout << "throughput: " << (records_per_bundle_test * bundle_per_interval_test * record_size)/elapsed_us/1024 << " GB/s" << std::endl;
			//std::cout << "throughput: " << (records_per_bundle_test * bundle_per_interval_test)/1024/(elapsed_us/1000/1000) << "KRec/s" << std::endl;
			std::cout << "throughput: " << (records_per_bundle_test * bundle_per_interval_test)/(elapsed_us/1000)/1024 << "MRec/s" << std::endl;
//////////////////////////////////////////////////////////////////////
#endif

#if 0
//test our own threads's scalability
//////////////////////////////////////////////////////////////////////////////////////////////////
		total_tasks = 64;
		std::cout << "record_num is " << t->record_num << std::endl;
		std::cout << "bundler_per_interval " << bundle_per_interval << std::endl;
		std::cout << "records_per_bundle " << records_per_bundle << std::endl;
		std::cout << "bundle_per_interval/total_tasks " << bundle_per_interval / total_tasks << std::endl;
		
		start_tick = boost::posix_time::microsec_clock::local_time();
		offset = 0;
		//for(int k = 0; k < 50; k++){ //move the loop inside the lambda function, reduce the times of pushing lambda functions to task queue, then performance increases.
			for (int task_id = 0; task_id < total_tasks; task_id++) {

				auto source_task_lambda = [t, &total_tasks, &bundle_per_interval,
				     this, &delta, &out, &c, &records_per_bundle, &num_nodes,
				     task_id, offset](int id)
				     {
					for(int k = 0; k < 50; k++){ //move here from outside of lambda function, reduce the times of pushing lambda functions to task queue, then performance increases
					     int64_t local_offset = offset + task_id * (t->record_num/ total_tasks);
					     for(unsigned int i = 0; i < t->record_num/total_tasks; i++){
					     		t->record_buffers[0][local_offset].data.first++;
							t->record_buffers[0][local_offset].data.second++;
							local_offset ++;
					     }
					}//end for k 
				     };//end lambda

				     if (task_id == total_tasks - 1) {
					     source_task_lambda(0);
					     continue;
				     }

				     futures.push_back( // store a future
						     c->executor_.push(source_task_lambda) /* submit to task queue */
						     );
			} //end for task

			for (auto && f : futures) {
				f.get();
			}
			futures.clear();
		//}//end for k

		boost::posix_time::ptime end_tick = 
				boost::posix_time::microsec_clock::local_time();
		auto elapsed_us = (end_tick - start_tick).total_microseconds();
		assert(elapsed_us > 0);
		std::cout << "----------------------" << std::endl;
		std::cout << "total_takes is: " << total_tasks << std::endl;
		std::cout << "record size: " << sizeof(t->record_buffers[0][0] ) << std::endl;
		std::cout << "total size: " << 50 * sizeof(t->record_buffers[0][0]) * t->record_num/1024/1024 << "MB" <<std::endl;
		std::cout << "total records: " << 50 * t->record_num << std::endl;
		std::cout << "total time: " << elapsed_us << " us" << std::endl;
		//std::cout << "tput is: " << sizeof(t->record_buffers[0][0] * t->record_num)/1024/1024/1024(elapsed_us/1000/1000) << "GB/s" << std::endl;
		std::cout << "tput is: " << 50 * sizeof(t->record_buffers[0][0]) * t->record_num/(elapsed_us * 1024) << "GB/s" << std::endl;
		std::cout << "tput is: " << 50 * t->record_num * 1000 /elapsed_us << "Rec/s" << std::endl;

/////////////////////////////////////////////////////////////////////////////////////////////////
#endif

#if 1
//test our own threads's scalability
//add: add recotds to bundle to see whether vector is the bottleneck
//////////////////////////////////////////////////////////////////////////////////////////////////
//		total_tasks = 64;
/*		std::cout << "default bundle_per_interval is " << bundle_per_interval << std::endl;
		std::cout << "default records_per_bundle is" << records_per_bundle << std::endl;

		std::cout << "record_num = " << t->record_num << std::endl;
		std::cout << "total_tasks = " << total_tasks << std::endl; 
		std::cout << "each task will process t->record_num/total_tasks=" << t->record_num/total_tasks << " records: REMEMBER TO *50 when calculate system throughput later!!!!" << std::endl;
		uint64_t local_bundle_per_interval = 2;
		uint64_t local_records_per_bundle = t->record_num/total_tasks/local_bundle_per_interval;
		std::cout << "local_bundle_per_interval = " << local_bundle_per_interval << " Remenber: this is local!!!!!!!!" << std::endl;
		std::cout << "local_records_per_bundle = " << local_records_per_bundle  << " Remember: this is local!!!!!!!!" << std::endl;

		std::cout << "local_bundler_per_intervl * local_records_per_bundle = " << local_bundle_per_interval * local_records_per_bundle << std::endl;
		std::cout << "make sure that: local_bundler_per_intervl * local_records_per_bundle == t->record_num/total_tasks" << std::endl;
*/
		start_tick = boost::posix_time::microsec_clock::local_time();
		offset = 0;
		//for(int k = 0; k < 50; k++){ //move the loop inside the lambda function, reduce the times of pushing lambda functions to task queue, then performance increases.

/* move it out of loop
		OutputBundleT ** bundle_ptr1 = new OutputBundleT * [2]; // = new shared_ptr<RecordBundle<pair<creek::ippair, long>>>[2];
		bundle_ptr1[0] = new OutputBundleT;
		bundle_ptr1[0]->add_record(t->record_buffers[0][0]);

*/

//have tried to use shared_ptr, but failed...
//Don't know how to use it in array...
#if 0
		shared_ptr<OutputBundleT> ** bundle_ptr2 = new shared_ptr<OutputBundleT> * [2]; // = new shared_ptr<RecordBundle<pair<creek::ippair, long>>>[2];
		//bundle_ptr2[0] = make_shared<OutputBundleT> *(new OutputBundleT);
		//bundle_ptr2[0] = make_shared<OutputBundleT>(new OutputBundleT);
		bundle_ptr2[0] = new OutputBundleT;
		bundle_ptr2[0]->add_record(t->record_buffers[0][0]);
#endif
		//int iteration = 12;
			for (int task_id = 0; task_id < total_tasks; task_id++) {

				auto source_task_lambda = [t, &total_tasks, &local_bundle_per_interval,
				     this, &delta, &out, &c, &local_records_per_bundle, &num_nodes,
				     task_id, offset, &iteration](int id)
				     {
					for(int k = 0; k < iteration; k++){ //move here from outside of lambda function, reduce the times of pushing lambda functions to task queue, then performance increases
					     int64_t local_offset = offset + task_id * (t->record_num/ total_tasks);
					     /*for(unsigned int i = 0; i < t->record_num/total_tasks; i++){
					     		t->record_buffers[0][local_offset].data.first++;
							t->record_buffers[0][local_offset].data.second++;
							local_offset ++;
					     }
					     */

					     OutputBundleT *tmp;
					     //tmp = c->get_bundle();
					     int nodeid = 0;
					     for(uint64_t m = 0; m < local_bundle_per_interval; m++){
/*							shared_ptr<outputbundlet> //outputbundlet is recordbundle<pair<creek::string, long>>;
								bundle(make_shared<outputbundlet>(
								local_records_per_bundle,
								nodeid));
							assert(bundle);
*/							
							//tmp = c->get_bundle();
/*							bundle_ptr2[k][task_id][m] = c->get_bundle();
							if(bundle_ptr2[k][task_id][m] == NULL){
								std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! no available bundle in pool !!!!!!!!!!!!!!!!!!!!!!" << std::endl;
							}
							assert(bundle_ptr2[k][task_id][m]);
					
*/
							//shared_ptr<OutputBundleT> *bundle = make_shared<OutputBundleT>(c->get_bundle());
							//shared_ptr<OutputBundleT> bundle(c->get_bundle());
							//shared_ptr<OutputBundleT> bundle((OutputBundleT *)out->consumer->get_bundle());
							//shared_ptr<OutputBundleT> bundle(out->consumer->get_bundle());
							//shared_ptr<OutputBundleT> bundle = make_shared<OutputBundleT> (out->consumer->get_bundle());
							shared_ptr<OutputBundleT> bundle = dynamic_pointer_cast<OutputBundleT> (out->consumer->get_bundle());
							if(bundle == NULL){
								std::cout << "!!!!!!!!!!!!!!!!!!!!!!!! no available bundle in pool!!!!!" << std::endl;
								abort();
							}

							//if(tmp != NULL){
							//	std::cout << "get one bundle ..." << std::endl; 
							//}
							//assert(tmp);

							//std::cout << "iteration: " << k << ", task_id: " << task_id << ", bundle: " << m << std::endl;
							
							ptime ts = current_ts + delta * m;
							for(uint64_t n = 0; n < local_records_per_bundle; n++){
					     			//t->record_buffers[0][local_offset].data.first++;
								//t->record_buffers[0][local_offset].data.second++;
								
								
								//just add records to bundle		
								//bundle->add_record(t->record_buffers[nodeid][local_offset]);
								//bundle_ptr1[task_id]->add_record(t->record_buffers[nodeid][local_offset]);
								
								//tmp->add_record(t->record_buffers[nodeid][local_offset]);
								
								//bundle_ptr2[k][task_id][m]->add_record(t->record_buffers[nodeid][local_offset]);
								
								/*
								//hym: this cause a write back to rcord_buffers, hurts performance much!(injection drops from 5.8 to 3.2MRec/s)
								//solution: assgin ts inside add_record(). define a new add_record(Record rec, ptime ts) in RecordBundle_test
								t->record_buffers[nodeid][local_offset].ts = current_ts + delta * m;
								bundle->add_record(t->record_buffers[nodeid][local_offset]);
								*/
								bundle->add_record(t->record_buffers[nodeid][local_offset], ts); //new add_record() defined in RecordBundle_teste
								local_offset ++;

							}
							//bundle_ptr2[k][task_id][m]->rewind();
							//bundle->rewind();
							
							out->consumer->depositOneBundle(bundle, nodeid);
							c->SpawnConsumer();
					     }

					}//end for k 
				     };//end lambda

				     if (task_id == total_tasks - 1) {
					     source_task_lambda(0);
					     continue;
				     }

				     futures.push_back( // store a future
						     c->executor_.push(source_task_lambda) /* submit to task queue */
						     );
			} //end for task

			for (auto && f : futures) {
				f.get();
			}
			futures.clear();
		//}//end for k

		t->byte_counter_.fetch_add(
					total_tasks * local_records_per_bundle * local_bundle_per_interval * iteration * sizeof(t->record_buffers[0][0]),
					std::memory_order_relaxed);

		t->record_counter_.fetch_add(
					total_tasks * local_records_per_bundle * local_bundle_per_interval * iteration,
					std::memory_order_relaxed);
		
		current_ts += punc_interval;
		current_ts += milliseconds(t->session_gap_ms);
		
		c->UpdateSourceWatermark(current_ts);

		/* Useful before the sink sees the 1st watermark */
		if (c->GetTargetWm() == max_date_time) { /* unassigned */
			c->SetTargetWm(current_ts);
		}

		static int wm_node = 0;
		out->consumer->depositOnePunc(
				make_shared<Punc>(current_ts, wm_node), wm_node);
		c->SpawnConsumer();

		
		boost::posix_time::ptime end_tick = 
				boost::posix_time::microsec_clock::local_time();

		//put bundles back to pool
/*		for(int i = 0; i < iteration; i++){
			for(int j = 0; j < total_tasks; j++){
				for(int k = 0; k < local_bundle_per_interval; k++){
					c->return_bundle(bundle_ptr2[i][j][k]);
				}
			}
		}
*/
		auto elapsed_us = (end_tick - start_tick).total_microseconds();
		assert(elapsed_us > 0);
#ifdef DEBUG_PRINT
		std::cout << "----------------------" << std::endl;
		std::cout << "total_takes is: " << total_tasks << std::endl;
		std::cout << "local_records_per_bundle is " << local_records_per_bundle;
		std::cout << "iteration is " << iteration << std::endl;
		std::cout << "bundle pool should be larger than: #whileloop * " << iteration * 2 * 64 << std::endl; 
		std::cout << "one iteration size: " << 64 * local_records_per_bundle * local_bundle_per_interval * sizeof(t->record_buffers[0][0] ) << std::endl;
		std::cout << "record size: " << sizeof(t->record_buffers[0][0] ) << std::endl;
		std::cout << "total size: #whilloop * " << iteration * sizeof(t->record_buffers[0][0]) * t->record_num/1024/1024 << "MB" <<std::endl;
		std::cout << "total records: " << iteration * t->record_num << std::endl;
		std::cout << "total time: " << elapsed_us << " us" << std::endl;
		//std::cout << "tput is: " << sizeof(t->record_buffers[0][0] * t->record_num)/1024/1024/1024/(elapsed_us/1000/1000) << "GB/s" << std::endl;
		//std::cout << "tput is: " << iteration * sizeof(t->record_buffers[0][0]) * t->record_num/(elapsed_us * 1024) << "GB/s" << std::endl;
#endif
		//std::cout << "tput is: " << iteration * t->record_num * 1000 * 1000 /elapsed_us << "Rec/s" << std::endl;

		std::cout << "tput is: " << (total_tasks/64.0) * iteration * t->record_num * 1000 * 1000 /elapsed_us << "Rec/s" << std::endl;
/////////////////////////////////////////////////////////////////////////////////////////////////
#endif




#if 0
////////////////////////////////////////////////////////////
//hym: original injection part
		for (int task_id = 0; task_id < total_tasks; task_id++) {

		    auto source_task_lambda = [t, &total_tasks, &bundle_per_interval,
		    		this, &delta, &out, &c, &records_per_bundle, &num_nodes,
				task_id, offset](int id)
		    {
			int64_t local_offset = offset + task_id * (bundle_per_interval * records_per_bundle / total_tasks);

			//ptime current_ts_1 = local_current_ts;
			//for (unsigned int i = 0; i < bundle_per_interval; i++) {
			for (unsigned int i = 0; i < bundle_per_interval / total_tasks; i++) {
				/* construct the bundles by reading NUMA buffers round-robin */
				int nodeid = (i % num_nodes);
				shared_ptr<OutputBundleT> //OutputBundleT is RecordBundle<pair<creek::string, long>>;
					bundle(make_shared<OutputBundleT>(
							records_per_bundle,
							nodeid));
				assert(bundle);
				for(unsigned int j = 0; j < records_per_bundle; j++, local_offset++){
					if((int64_t)local_offset >= t->record_num){
						local_offset = 0; //wrap around, should be atomic if use multithreads in source
					}

					t->record_buffers[nodeid][local_offset].ts  
						= current_ts + delta * i;
#if 0
					bundle->add_record(
						Record<pair<creek::string, long>>(
							pair<creek::string, long>(
								t->record_buffers[nodeid][offset].data.sd_ip,
								t->record_buffers[nodeid][offset].data.rtt
							), //data: pair
							current_ts + delta * i //ts
						)//Record(data, ts)
					);
#endif
					bundle->add_record(t->record_buffers[nodeid][local_offset]);
				}
				
				out->consumer->depositOneBundle(bundle, nodeid);
				c->SpawnConsumer();
			}//end for
		    };//end lambda

		    if (task_id == total_tasks - 1) {
		    	source_task_lambda(0);
			continue;
		    }

		    futures.push_back( // store a future
		    	c->executor_.push(source_task_lambda) /* submit to task queue */
		    );

		}//end for task
			
			offset += records_per_bundle * bundle_per_interval;
			//offset %= t->buffer_size_records;
			if(offset >= t->buffer_size_records){
				offset = offset - t->buffer_size_records;
			}

			for (auto && f : futures) {
				f.get();
			}
			futures.clear();

	
			//////////////////////// stop here//////////////////////////////////////////////////////////////
			t->byte_counter_.fetch_add(
					t->records_per_interval * t->record_len,
					std::memory_order_relaxed);

			t->record_counter_.fetch_add(
					t->records_per_interval,
					std::memory_order_relaxed);

			boost::posix_time::ptime end_tick = 
				boost::posix_time::microsec_clock::local_time();
			auto elapsed_us = (end_tick - start_tick).total_microseconds();
			assert(elapsed_us > 0);
			
			std::cout << "elapsed_us: " << elapsed_us << " us" << std::endl;
			std::cout << "records: " << records_per_bundle * bundle_per_interval << std::endl;
			//std::cout << "inject speed is " << 
			//std::cout <<"inject speed is " << (records_per_bundle * bundle_per_interval * 40)/1024/1024/(elapsed_us/1000) <<"MB/s" << std::endl;
			
			if ((unsigned long)elapsed_us > us_per_iteration)
				EE("warning: source runs at full speed.");
			else {
				usleep(us_per_iteration - elapsed_us);
				EE("source pauses for %lu us", us_per_iteration - elapsed_us);
			}

			current_ts += punc_interval;
			current_ts += milliseconds(t->session_gap_ms);

			c->UpdateSourceWatermark(current_ts);

			/* Useful before the sink sees the 1st watermark */
			if (c->GetTargetWm() == max_date_time) { /* unassigned */
				c->SetTargetWm(current_ts);
			}

			static int wm_node = 0;
			out->consumer->depositOnePunc(
				make_shared<Punc>(current_ts, wm_node), wm_node);
			c->SpawnConsumer();
//hym: end of original injection code
////////////////////////////////////////////////
#endif
		}//end while
/*
		for(int i = 0; i < 64; i++){
			//bundle_ptr1[i] = new OutputBundleT;
			delete bundle_ptr1[i];
		}
		delete[] bundle_ptr1;
*/
	}//end evaluate


};// end NetworkLatencySourceEvaluator
#endif /* NETWORKLATENCYSOURCEEVALUATOR_H */
