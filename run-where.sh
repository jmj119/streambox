# if using adavanced thread scheduling, then just set source_threads to 60
# scheduler will make the balance between ingestion and computation
# note: 60 seems a good number, if source_threads is set to 64, the performance becomes much worse
#       so we should set an approproate number later...
./test-where.bin --source_threads 64
