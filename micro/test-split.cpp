/*
 * split a string
 * compile: g++ --std=c++11 -o test-split test-split.cpp
 * */

#include <boost/algorithm/string.hpp>
#include<stdlib.h>
#include<iostream>
int main(){
	std::string text = "Let me split this into words";
	std::vector<std::string> results;

	std::string text1 = "Let	what	a	who";

	boost::split(results, text, [](char c){return c == ' ';});
	//std::cout << results << std::endl;
	for(auto i = results.begin(); i != results.end(); i++){
		std::cout << *i << std::endl;
	}
	
	boost::split(results, text1, [](char c){return c == '\t';});

	for(auto i = results.begin(); i != results.end(); i++){
		std::cout << *i << std::endl;
	}
	return 0;
}



