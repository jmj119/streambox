/*
 * Sink.h
 *
 * all kinds of sink defs
 *
 *  Created on: Sept 16, 2016
 *      Author: xzl
 */

#ifndef _SINK_H
#define _SINK_H

#include <iostream>

#include "config.h"

extern "C" {
#include "log.h"
#include "measure.h"
}

#include "core/Transforms.h"

using namespace std;

///////////////////////////////////////////////////////

/* A bunch of records. This is good for that each window outputs a single value
 * @T: element type, e.g. long */
template<class T>
class RecordBundleSink : public PTransform {
    using InputBundleT = RecordBundle<T>;

public:
    RecordBundleSink(string name) : PTransform(name) { }

    static void printBundle(const InputBundleT & input_bundle) {
        I("got one bundle");
    }

    void ExecEvaluator(int nodeid, EvaluationBundleContext *c,
    		shared_ptr<BundleBase> bundle_ptr) override;

};

///////////////////////////////////////////////////////

/* @T: element type, e.g. long */
template<class T>
class RecordBitmapBundleSink : public PTransform {
    using InputBundleT = RecordBitmapBundle<T>;

public:
    RecordBitmapBundleSink(string name) : PTransform(name) { }

    /* default behavior.
     * can't do const. see below */
    static void printBundle(InputBundleT & input_bundle) {
        I("got one bundle");
    }

    void ExecEvaluator(int nodeid, EvaluationBundleContext *c,
    		shared_ptr<BundleBase> bundle_ptr) override;

    static bool report_progress(InputBundleT & input_bundle){ return false;} //hym: defined in Sink.cpp

private:
    //hym: for measuring throughput(report_progress())
    static uint64_t total_bytes;
    static uint64_t total_records;
    static uint64_t last_bytes;
    static uint64_t last_records;
    static ptime last_check;
    static ptime start_time;
    static int once;
#if 0
    /* internal accounting */
    uint64_t total_bytes = 0, total_records = 0;
    /* last time we report */
    uint64_t last_bytes = 0, last_records = 0;
    ptime last_check, start_time;
    int once = 1;
#endif
};
template<class T>
uint64_t RecordBitmapBundleSink<T>::total_bytes = 0;
template<class T>
uint64_t RecordBitmapBundleSink<T>::total_records = 0;
template<class T>
uint64_t RecordBitmapBundleSink<T>::last_bytes = 0;
template<class T>
uint64_t RecordBitmapBundleSink<T>::last_records = 0;

template<class T>
ptime RecordBitmapBundleSink<T>::last_check = boost::posix_time::microsec_clock::local_time();
template<class T>
ptime RecordBitmapBundleSink<T>::start_time = boost::posix_time::microsec_clock::local_time();;

template<class T>
int RecordBitmapBundleSink<T>::once = 1;
///////////////////////////////////////////////////////

template<class T>
class WindowsBundleSink : public PTransform {
	using InputBundleT = WindowsBundle<T>;

public:
	WindowsBundleSink(string name)  : PTransform(name) { }

  static void printBundle(const InputBundleT & input_bundle) {
      I("got one bundle");
  }

  void ExecEvaluator(int nodeid, EvaluationBundleContext *c,
  		shared_ptr<BundleBase> bundle_ptr) override;

};

//for where
template<class T>
class WhereBundleSink : public PTransform{
	//using InputBundleT = RecordBundle<T>;
	using InputBundleT = RecordBundle_test;
	using LocalBundleT = RecordBundle_test;
	using RecordT = Record<clusterid_rtt>;
public:
	//list<shared_ptr<LocalBundleT>> bundles;
	int pool_size = 2500;
	int get = 0;
	int put = 0;
	list<shared_ptr<LocalBundleT>> bundles_fast; //on fast memory
	list<shared_ptr<LocalBundleT>> bundles_slow; //on slow memory 
	//std::mutex mutex_pool;
	std::mutex mutex_pool_fast;
	std::mutex mutex_pool_slow;
	int threshold_slow = 50;
	
	WhereBundleSink(string name) : PTransform(name){
		int i;
		for(i = 0; i < pool_size; i++){
			//LocalBundleT * tmp = new LocalBundleT;
			shared_ptr<LocalBundleT> tmp (new LocalBundleT);
			tmp->content_array = (RecordT *) hbw_malloc(sizeof(RecordT) * 114450);
			if(tmp->content_array == NULL){
				std::cout << "hbw_malloc failed...." << std::endl;
				abort();
			}
			tmp->on_fast_mem = 1;
			
			bundles_fast.push_back(tmp);	
		}
		std::cout << "pre-allocate " << i << " bundles for WhereBundleSink on fast memory" << std::endl; 
	
		for(i = 0; i < pool_size; i++){
			//LocalBundleT * tmp = new LocalBundleT;
			shared_ptr<LocalBundleT> tmp (new LocalBundleT);
			//tmp->content_array = new Record<clusterid_rtt>[114450];
			tmp->content_array = (RecordT *) malloc(sizeof(RecordT) * 114450);
			if(tmp->content_array == NULL){
				std::cout << "malloc failed...." << std::endl;
				abort();
			}
			tmp->on_fast_mem = 0;
			bundles_slow.push_back(tmp);	
		}
		std::cout << "pre-allocate " << i << " bundles for WhereBundleSink on slow memory" << std::endl;
	
	}
	~WhereBundleSink(){
		int num = 0;
		while(!bundles_fast.empty()){
			bundles_fast.pop_front();
			num++;
		}
		std::cout << "release " << num << " bundles in ~WhereBundleSink fast memory" << std::endl;
		num = 0;
		while(!bundles_slow.empty()){
			bundles_slow.pop_front();
			num++;
		}
		std::cout << "release " << num << " bundles in ~WhereBundleSink slow memory" << std::endl;
	}


	shared_ptr<BundleBase>  get_bundle_fast(){
		std::unique_lock<std::mutex> lock(this->mutex_pool_fast);
		if(bundles_fast.empty()){
			//std::cout << "bundle list in WhereBundleSink is empty!" << std::endl;
			return nullptr;
			//abort();	
		}
		//LocalBundleT *p = bundles.front();
		shared_ptr<LocalBundleT> p = bundles_fast.front();
		bundles_fast.pop_front();
		get ++;
		p->rewind();
		//std::cout << "get one bundle from Where's pool" << std::endl;
#ifdef DEBUG_PRINT
		std::cout << "get " << get << "bundle from WhereBundleSink's pool" << std::endl;
#endif
		shared_ptr<BundleBase> t = dynamic_pointer_cast<BundleBase>(p);
		return t;
	}

	shared_ptr<BundleBase>  get_bundle_slow() {
		std::unique_lock<std::mutex> lock(this->mutex_pool_slow);
		
		if(bundles_slow.size() < this->threshold_slow){
			for(int i = 0; i < threshold_slow; i++){
				//LocalBundleT * tmp = new LocalBundleT;
				shared_ptr<LocalBundleT> tmp (new LocalBundleT);
				//tmp->content_array = new Record<clusterid_rtt>[114450];
				tmp->content_array = (RecordT *) malloc(sizeof(RecordT) * 114450);
				if(tmp->content_array == NULL){
					std::cout << "malloc failed...." << std::endl;
					abort();
				}
				tmp->on_fast_mem = 0;
				bundles_slow.push_back(tmp);	
			}
		}
/*
		if(bundles_slow.empty()){
			std::cout << "bundle list in Where is empty!" << std::endl;
			abort();	
		}
*/
		//LocalBundleT *p = bundles.front();
		shared_ptr<LocalBundleT> p = bundles_slow.front();
		bundles_slow.pop_front();
		get ++;
		p->rewind();
		//std::cout << "get one bundle from Where's pool" << std::endl;
#ifdef DEBUG_PRINT
		std::cout << "get " << get << "bundle from WhereBundleSink's pool" << std::endl;
#endif
		shared_ptr<BundleBase> t = dynamic_pointer_cast<BundleBase>(p);
		return t;
	}

	
	shared_ptr<BundleBase>  get_bundle() override{
		shared_ptr<BundleBase> ret;
		ret = get_bundle_fast();
		if(ret == nullptr){
			ret = get_bundle_slow();
		}
		return ret;
	}

	
	void return_bundle_fast(shared_ptr<BundleBase> p){
		std::unique_lock<std::mutex> lock(this->mutex_pool_fast);
		shared_ptr<LocalBundleT> t = dynamic_pointer_cast<LocalBundleT>(p);
		bundles_fast.push_back(t);
		put++;
#ifdef DEBUG_PRINT
		std::cout << "put " << put << "bundle from WhereBundleSink's pool" << std::endl;
#endif
	}

	void return_bundle_slow(shared_ptr<BundleBase> p){
		std::unique_lock<std::mutex> lock(this->mutex_pool_slow);
		shared_ptr<LocalBundleT> t = dynamic_pointer_cast<LocalBundleT>(p);
		bundles_slow.push_back(t);
		put++;
#ifdef DEBUG_PRINT
		std::cout << "put " << put << "bundle from WhereBundleSink's pool" << std::endl;
#endif
	}

	void return_bundle(shared_ptr<BundleBase> p) override{
		if(p->on_fast_mem == 1){
			return_bundle_fast(p);
		}else{
			return_bundle_slow(p);
		}
	}	


	static void printBundle(const InputBundleT & input_bundle){
		I("got one bundle");
	}
	void ExecEvaluator(int nodeid, EvaluationBundleContext *c,
		shared_ptr<BundleBase> bundle_ptr) override;
};

//for WinGBK_test
template<class T>
class WinGBKBundleSink : public PTransform{
	//using InputBundleT = RecordBundle<T>;
	using InputBundleT = WindowsKeyedBundle_test;
	using LocalBundleT = WindowsKeyedBundle_test;
public:

	WinGBKBundleSink(string name) : PTransform(name){}

	static void printBundle(const InputBundleT & input_bundle){
		I("got one bundle");
	}
	void ExecEvaluator(int nodeid, EvaluationBundleContext *c,
		shared_ptr<BundleBase> bundle_ptr) override;
};
#endif // _SINK_H
