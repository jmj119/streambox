#ifndef WINGBKBUNDLESINK_CPP
#define WINGBKBUNDLESINK_CPP


#include "WinGBKBundleSinkEvaluator.h"

/* ----  specialization: different ways to dump bundle contents  --- */

/* for simple where */
template<>
/*
void WhereBundleSink<pair<creek::ippair, long>>::printBundle
	(const RecordBundle<pair<creek::ippair, long>> & input_bundle) {
*/
void WinGBKBundleSink<clusterid_rtt>::printBundle
	//(const RecordBundle<clusterid_rtt> & input_bundle) {
	(const WindowsKeyedBundle_test & input_bundle) {
    W("got one bundle: ");
#if 0
#ifndef NDEBUG
    for (auto && win_frag: input_bundle.vals) {
    	auto && win = win_frag.first;
    	auto && pfrag = win_frag.second;
    	cout << "==== window ===== " << endl;
    	cout << to_simplest_string1(win.window_start()).str() << endl;
    	for (auto && rec : pfrag->vals) {
    		cout << rec.data.first << ": " << rec.data.second << endl;
    	}
    	cout << "----------------" << endl;
    }
#endif
#endif
}


/* -------------------- ExecEvaluator --------------------
 * out of line to avoid circular dependency
 */


template<class T>
void WinGBKBundleSink<T>::ExecEvaluator(
		int nodeid, EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr)
{

#ifndef NDEBUG /* if evaluators get stuck ...*/
	static atomic<int> outstanding (0);
#endif
	WinGBKBundleSinkEvaluator<T> eval(nodeid);
	//eval.evaluate(this, c, bundle_ptr);

#ifndef NDEBUG		// for debug
//	I("begin eval...");
	outstanding ++;
#endif
  
	eval.evaluate(this, c, bundle_ptr);

#ifndef NDEBUG   // for debug
	outstanding --; I("end eval... outstanding = %d", outstanding);
#endif
}


/* ---- instantiation for concrete types --- */


/* for netmon simple where*/
/*
template
void WhereBundleSink<pair<creek::ippair, long>>::ExecEvaluator(
		int nodeid, EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);
*/
template
void WinGBKBundleSink<clusterid_rtt>::ExecEvaluator(
		int nodeid, EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);

#endif /* WINGBKBUNDLESINK_CPP */
