#ifndef WINGBKSINKEVALUATOR_H
#define WINGBKSINKEVALUATOR_H

#include "core/SingleInputTransformEvaluator.h"
#include "Sink/Sink.h"

/* InputT: the element type of the record bundle */
template <typename InputT>
class WinGBKBundleSinkEvaluator
    : public SingleInputTransformEvaluator<WinGBKBundleSink<InputT>,
//      WindowsKeyedBundle<InputT>, WindowsBundle<InputT>> {
      //RecordBundle<InputT>, RecordBundle<InputT>> {   // sufficient for wc & topK?
      WindowsKeyedBundle_test, WindowsKeyedBundle_test> {   // sufficient for wc & topK?

	using TransformT = WinGBKBundleSink<InputT>;
	//using InputBundleT = RecordBundle<InputT>;
	using InputBundleT = WindowsKeyedBundle_test;
//	using InputBundleT = WindowsKeyedBundle<InputT>;
	//using InputBundleT = WindowsKeyedBundle<KVPair>;
	//using OutputBundleT = RecordBundle<InputT>;
	using OutputBundleT = WindowsKeyedBundle_test;

public:

	WinGBKBundleSinkEvaluator(int node)
	: SingleInputTransformEvaluator<TransformT, InputBundleT, OutputBundleT>(node) { }

  bool evaluateSingleInput (TransformT* trans,
        shared_ptr<InputBundleT> input_bundle,
        shared_ptr<OutputBundleT> output_bundle) override {
#ifdef DEBUG_PRINT
	std::cout << "WinGBK sink get one bundle!!!!!!!!!!!!!!!" << std::endl;
#endif
	//shared_ptr<BundleBase> t = dynamic_pointer_cast<BundleBase> (input_bundle);
	//trans->return_bundle(t);

    //XXX TransformT::printBundle(*input_bundle);
    //TransformT::report_progress(* input_bundle);
    return false; /* no output bundle */
  }

};

#endif /* WINGBKSINKEVALUATOR_H */
