/*
 * simple where, just process integer
 * Record<pair<creek::ippair, long>>
 * */

#include "core/SingleInputTransformEvaluator.h"

template<class KVPair>
// template<class TransformT, class InputBundleT, class OutputBundleT>
class WhereEvaluator 
	: public SingleInputTransformEvaluator<
		Where<KVPair>,
		//RecordBundle<KVPair>,
		//RecordBundle<KVPair> //or RecordBundle<long>, or no output	
		RecordBundle_test,
		RecordBundle_test //or RecordBundle<long>, or no output	
		>{
	using TransformT = Where<KVPair>;
	using RecordKV = Record<KVPair>;
	//using InputBundleT = RecordBundle<KVPair>;
	using InputBundleT = RecordBundle_test;
	//using InputBundleT = RecordBundle<pair<creek::ippair, long>>;
	//using OutputBundleT = RecordBundle<KVPair>; //not good here
	using OutputBundleT = RecordBundle_test; //not good here
	//using OutputBundleT = RecordBundle<pair<creek::ippair, long>>;
	//using K = decltype(KVPair:first);
	//using V = decltype(KVPair::second);

public:


	bool evaluateSingleInput(TransformT* trans,
		shared_ptr<InputBundleT> input_bundle,
		shared_ptr<OutputBundleT> output_bundle) override{
#ifdef DEBUG_PRINT
		std::cout << "in Where's evaluateSingleInput() " << std::endl;
#endif
		//std::cout  << "in WhereEvaluator's evaluateSingleInput, remember to modify this function !!!!!!!!!!!" << std::endl;
		//std::cout << "input_bundle's ptr is" << input_bundle->ptr << std::endl;
		//std::cout << "evaluateSingleInput should return false if this transform doesn't have downstream transforms" << std::endl;
		
		//input_bundle->c->return_bundle(input_bundle);
		int i;
		int total_num = 0;
		for(i = 0; i < input_bundle->index; i++){
			if(input_bundle->content_array[i].data.rtt > 500){
				output_bundle->add_record(input_bundle->content_array[i]);
				total_num ++;
			}
		}

#ifdef DEBUG_PRINT
		std::cout << "index is " << input_bundle->index << std::endl;
		std::cout << "scan " << i << " records in Where's input bundle" << std::endl;
#endif
		//trans->return_bundle((BundleBase *)input_bundle.get());
		
		shared_ptr<BundleBase> t = dynamic_pointer_cast<BundleBase> (input_bundle);
		//trans->return_bundle(input_bundle);
		trans->return_bundle(t);
		//trans->return_bundle((BundleBase *)output_bundle.get());

/*		
		//bool ret = false;
		for(auto && it = input_bundle->begin();
			it != input_bundle->end(); ++it){
			//std::cout << "WhereEvaluator's evaluateSingleInput" << std::endl;
			//std::cout << "WhereEvaluator's evaluateSingleInput: k is " << (*it).data.first << ", v is " << (*it).data.second << ", ts is" << (*it).ts <<std::endl; 	
			
			//this is just for test
			if((*it).data.second > 1000){
			//if((*it).data.rtt > 1000){
				//std::cout << "rtt > 1000, output to downstream.........." << std::endl;
				output_bundle->add_record(*it);
			}
		}

*/

		return true; //if there is downstream transform
		//return false; //if there is no downstream transform
		
	}
WhereEvaluator(int node)
		: SingleInputTransformEvaluator<TransformT,
			InputBundleT, OutputBundleT>(node) { }
}; //end WhereEvaluator
