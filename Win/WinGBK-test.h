/*
 * Stateless. a combo of windowing and grouping by key. since both transforms
 * are re-organizing the records, each will have to touch & move every record.
 * Thus, it's more reasonable to combine the two transforms.
 */
#ifndef WINGBK_H
#define WINGBK_H

#include "core/Transforms.h"

// input bundle: RecordBundle or RecordBitmapBUndle
// output bundle: WindowsKeyedBundle
#if 0
template <class KVPair,
					template<class> class InputBundleT,
					template<class> class WindowKeyedFragmentT  /* don't have to be MT safe */
					>
#endif
template <class KVPair>
class WinGBK_test : public PTransform {
  //using InputT = Record<KVPair>;
  using InputT = Record<clusterid_rtt>;
  using RecordT = Record<clusterid_rtt>;
  //using LocalBundleT = WindowsKeyedBundle_test;
  using LocalBundleT = RecordBundle_test;

public:
	//list<LocalBundleT *> bundles;
	//list<shared_ptr<LocalBundleT>> bundles;
	list<shared_ptr<LocalBundleT>> bundles_fast; //on fast memory
	list<shared_ptr<LocalBundleT>> bundles_slow; //on slow memory 
	int pool_size = 2500;
	int get = 0;
	int put = 0;
	//std::mutex mutex_pool;
	std::mutex mutex_pool_fast;
	std::mutex mutex_pool_slow;
	int threshold_slow = 50;
  
  const boost::posix_time::time_duration window_size;
  const ptime start; // the starting point of windowing.
  WinGBK_test(string name,
      boost::posix_time::time_duration window_size,
      ptime start = Window::epoch)
    : PTransform(name), window_size(window_size), start(start) { 
		
		int i;
		/*
		for(i = 0; i < pool_size; i++){
			//LocalBundleT * tmp = new LocalBundleT;
			shared_ptr<LocalBundleT> tmp (new LocalBundleT);
			bundles.push_back(tmp);	
		}
		std::cout << "pre-allocate " << i << " bundles for WinGBK_test" << std::endl; 
    		*/
		for(i = 0; i < pool_size; i++){
			//LocalBundleT * tmp = new LocalBundleT;
			shared_ptr<LocalBundleT> tmp (new LocalBundleT);
			//tmp->content_array = new Record<clusterid_rtt>[114450];
			tmp->content_array = (RecordT *) hbw_malloc(sizeof(RecordT) * 114450);
			if(tmp->content_array == NULL){
				std::cout << "hbw_malloc failed...." << std::endl;
				abort();
			}
			tmp->on_fast_mem = 1;
			bundles_fast.push_back(tmp);	
		}
		std::cout << "pre-allocate " << i << " bundles for WinGBK_test on fast memory" << std::endl;
    		
		for(i = 0; i < pool_size; i++){
			//LocalBundleT * tmp = new LocalBundleT;
			shared_ptr<LocalBundleT> tmp (new LocalBundleT);
			//tmp->content_array = new Record<clusterid_rtt>[114450];
			tmp->content_array = (RecordT *) malloc(sizeof(RecordT) * 114450);
			if(tmp->content_array == NULL){
				std::cout << "malloc failed...." << std::endl;
				abort();
			}
			tmp->on_fast_mem = 0;
			bundles_slow.push_back(tmp);	
		}
		std::cout << "pre-allocate " << i << " bundles for WinGBK_test on slow memory" << std::endl;
    }


	shared_ptr<BundleBase>  get_bundle_fast(){
		std::unique_lock<std::mutex> lock(this->mutex_pool_fast);
		if(bundles_fast.empty()){
			//std::cout << "bundle list in WinGBK_test is empty!" << std::endl;
			return nullptr;
			//abort();	
		}
		//LocalBundleT *p = bundles.front();
		shared_ptr<LocalBundleT> p = bundles_fast.front();
		bundles_fast.pop_front();
		get ++;
		p->rewind();
		//std::cout << "get one bundle from Where's pool" << std::endl;
#ifdef DEBUG_PRINT
		std::cout << "get " << get << "bundle from WinGBK_test pool" << std::endl;
#endif
		shared_ptr<BundleBase> t = dynamic_pointer_cast<BundleBase>(p);
		return t;
	}
	
	shared_ptr<BundleBase>  get_bundle_slow() {
		std::unique_lock<std::mutex> lock(this->mutex_pool_slow);
		
		if(bundles_slow.size() < this->threshold_slow){
			for(int i = 0; i < threshold_slow; i++){
				//LocalBundleT * tmp = new LocalBundleT;
				shared_ptr<LocalBundleT> tmp (new LocalBundleT);
				//tmp->content_array = new Record<clusterid_rtt>[114450];
				tmp->content_array = (RecordT *) malloc(sizeof(RecordT) * 114450);
				if(tmp->content_array == NULL){
					std::cout << "malloc failed...." << std::endl;
					abort();
				}
				tmp->on_fast_mem = 0;
				bundles_slow.push_back(tmp);	
			}
		}
		
		shared_ptr<LocalBundleT> p = bundles_slow.front();
		bundles_slow.pop_front();
		get ++;
		p->rewind();
#ifdef DEBUG_PRINT
		std::cout << "get " << get << "bundle from Where's pool" << std::endl;
#endif
		shared_ptr<BundleBase> t = dynamic_pointer_cast<BundleBase>(p);
		return t;
	}

	shared_ptr<BundleBase>  get_bundle() override{
		shared_ptr<BundleBase> ret;
		ret = get_bundle_fast();
		if(ret == nullptr){
			ret = get_bundle_slow();
		}
		return ret;
	}



	void return_bundle_fast(shared_ptr<BundleBase> p){
		std::unique_lock<std::mutex> lock(this->mutex_pool_fast);
		shared_ptr<LocalBundleT> t = dynamic_pointer_cast<LocalBundleT>(p);
		bundles_fast.push_back(t);
		put++;
#ifdef DEBUG_PRINT
		std::cout << "put " << put << "bundle to WinGBK_test's pool" << std::endl;
#endif
	}

	void return_bundle_slow(shared_ptr<BundleBase> p){
		std::unique_lock<std::mutex> lock(this->mutex_pool_slow);
		shared_ptr<LocalBundleT> t = dynamic_pointer_cast<LocalBundleT>(p);
		bundles_slow.push_back(t);
		put++;
#ifdef DEBUG_PRINT
		std::cout << "put " << put << "bundle to WinGBK_test's pool" << std::endl;
#endif
	}

	void return_bundle(shared_ptr<BundleBase> p) override{
		if(p->on_fast_mem == 1){
			return_bundle_fast(p);
		}else{
			return_bundle_slow(p);
		}
	}	

	~WinGBK_test(){
		int num = 0;
		while(!bundles_fast.empty()){
			bundles_fast.pop_front();
			num++;
		}
		std::cout << "release " << num << " bundles in ~WinGBK_test on fast memory" << std::endl;
		
		num = 0;
		while(!bundles_slow.empty()){
			bundles_slow.pop_front();
			num++;
		}
		std::cout << "release " << num << " bundles in ~WinGBK_test on slow memory" << std::endl;
	}

/*	~WinGBK_test(){
		int num = 0;
		while(!bundles.empty()){
			bundles.pop_front();
			num++;
		}
		std::cout << "release " << num << " bundles in ~WinGBK" << std::endl;
	}
	
	BundleBase * get_bundle() override{
		std::unique_lock<std::mutex> lock(this->mutex_pool);
		if(bundles.empty()){
			std::cout << "bundle list in Where is empty!" << std::endl;
			abort();	
		}
		LocalBundleT *p = bundles.front();
		bundles.pop_front();
		get ++;
		//p->rewind();
		std::cout << "get one bundle from WinGBK_test's pool" << std::endl;
		return (BundleBase *)p;
	}
	void return_bundle(BundleBase *p) override{
		std::unique_lock<std::mutex> lock(this->mutex_pool);
		bundles.push_back((LocalBundleT *)p);
		put++;
		std::cout << "put back to WinGBK_test's bundle pool" << std::endl;
	}
*/
  void ExecEvaluator(int nodeid, EvaluationBundleContext *c,
  			shared_ptr<BundleBase> bundle_ptr) override;

//  void ExecEvaluator(int nodeid, EvaluationBundleContext *c) override {
//  	/* instantiate an evaluator */
//  	WinGBKEvaluator<KVPair> eval(nodeid);
//		eval.evaluate(this, c);
//  }

};

#endif // WINGBK_H
