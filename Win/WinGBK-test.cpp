/*
 * WinGBK.cpp
 *
 *  Created on: Sep 25, 2016
 *      Author: xzl
 *
 *  Purdue University, 2016
 */

#define K2_NO_DEBUG 1

#include "Values.h"
#include "core/EvaluationBundleContext.h"
#include "WinGBK-test.h"
#include "WinGBKEvaluator-test.h"

/*
template <class KVPair,
					template<class> class InputBundleT,
					template<class> class WindowKeyedFragmentT
					>
*/
template <class KVPair>
//void WinGBK<KVPair, InputBundleT, WindowKeyedFragmentT>::ExecEvaluator(int nodeid,
void WinGBK_test<KVPair>::ExecEvaluator(int nodeid,
			EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr) {

#ifndef NDEBUG /* if evaluators get stuck ...*/
	static atomic<int> outstanding (0);
#endif
	/* instantiate an evaluator */
	//WinGBKEvaluator_test<KVPair, InputBundleT, WindowKeyedFragmentT> eval(nodeid);
	WinGBKEvaluator_test<KVPair> eval(nodeid);
	//eval.evaluate(this, c);

#ifndef NDEBUG		// for debug
	outstanding ++;
#endif
	eval.evaluate(this, c, bundle_ptr);

#ifndef NDEBUG   // for debug
	outstanding --; I("end eval... outstanding = %d", outstanding);
#endif
}

/* template instantiation with concrete types. NB: MT-safe WinKeyFrag often overkill. */

template
void WinGBK_test<clusterid_rtt>
	::ExecEvaluator(int nodeid,
									EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);
/* ---- for netmon --- */
/*
template
void WinGBK<pair<creek::ippair, long>, RecordBundle, WinKeyFragLocal_Simple>
	::ExecEvaluator(int nodeid,
									EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);
*/
/* ---- for wordcount --- */
/*
template
//void WinGBK<pair<creek::string, long>, RecordBundle<pair<std::string, long>>>
void WinGBK<pair<creek::string, long>, RecordBundle, WinKeyFragLocal_Std>
	::ExecEvaluator(int nodeid,
									EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);
*/
/* todo: instantiate more types */
/*
template
//void WinGBK<pair<creek::string, long>, RecordBundle<pair<std::string, long>>>
void WinGBK<clusterid_rtt, RecordBundle_test, WinKeyFragLocal_Std>
	::ExecEvaluator(int nodeid,
									EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);
*/
/*
template
//void WinGBK<pair<creek::string, long>, RecordBundle<pair<std::string, long>>>
void WinGBK<clusterid_rtt, RecordBundle_test, WinKeyFragLocal_Std>
	::ExecEvaluator(int nodeid,
									EvaluationBundleContext *c, shared_ptr<BundleBase> bundle_ptr);
									*/
