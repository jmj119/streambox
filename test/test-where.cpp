#include "core/Pipeline.h"
#include "Source/NetworkLatencySource.h"
#include "Values.h"
#include "test-common.h"
#include "Source/NetworkLatencySourceEvaluator.h"
#include "Select/Where.h"
#include "Select/WhereEvaluator.h"
#include "Sink/RecordBundleSinkEvaluator.h"
#include "Sink/WhereBundleSinkEvaluator.h"
#include "Win/WinGBKEvaluator-test.h"
#include "Sink/WinGBKBundleSinkEvaluator.h"

template<class T>
using BundleT = RecordBundle<T>;

#ifdef DEBUG
pipeline_config config = {
	.records_per_interval = 10 * 1000,
	.target_tput = (200 * 1000),
	.record_size = sizeof(struct srcdst_rtt),
	.input_file = "/ssd/region_Raw_PingmeshData.result",
	.source_threads = 1,
};
#else
pipeline_config config = {
	//.records_per_interval = 500 * 1000,
	.records_per_interval = 1000 * 1000 * 10,
	.target_tput = 800 * 1000,
	//.record_size = sizeof(struct srcdst_rtt),
	.record_size = 216, //sizeof(PingmeshRecord)
	//.input_file = "/ssd/region_Raw_PingmeshData.result",
	//.input_file = "/ssd/region_Raw_PingmeshData_100.log",
	.input_file = "/ssd/input_1.0.log",
	.source_threads = 1,
};
#endif

int main(int ac, char *av[]){
	parse_options(ac, av, &config);
	print_config();

	NetworkLatencySource netsource(
		"[netsource]",
		config.input_file.c_str(),
		config.records_per_interval , /* records per wm interval */
		config.target_tput,           /* target tput (rec/sec) */
		config.record_size,
		0, //XXX session_gap_ms = ???? Don't need it here
		config.source_threads);
	Pipeline* p = Pipeline::create(NULL);
	PCollection *netsource_output = dynamic_cast<PCollection *>(p->apply1(&netsource));
	netsource_output->_name = "src_out";
	
	//Where<pair<creek::ippair,long>> where("[where]");
	Where<clusterid_rtt> where("[where]");
	connect_transform(netsource, where);
	
/*	
	WhereBundleSink<clusterid_rtt> sink("[Sink]");
	connect_transform(where, sink);
*/	

	WinGBK_test<clusterid_rtt> wgbk ("[wingbk]", seconds(1));
	connect_transform(where, wgbk);

	WinGBKBundleSink<clusterid_rtt> sink("[Sink]");
	connect_transform(wgbk, sink);

	EvaluationBundleContext eval(1, config.cores);
	eval.runSimple(p);

	return 0;
}




