#ifndef RECORD_TYPE_H
#define RECORD_TYPE_H
typedef struct clusterid_rtt{
	long clusterid;
	long rtt;
	clusterid_rtt& operator=(const clusterid_rtt& rhs){
		clusterid = rhs.clusterid;
		rtt = rhs.rtt;
		return *this;
	}
} clusterid_rtt;

#endif /* RECORD_TYPE_H */
